﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content.PM;

namespace MemeCookie
{
    [Activity(Label = "MemeCookie", MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        private int _clicks;
        private ImageButton _cookieImage;
        private TextView _cookieText;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            _clicks = 0;
            _cookieImage = FindViewById<ImageButton>(MemeCookie.Resource.Id.cookieImage);
            _cookieText = FindViewById<TextView>(MemeCookie.Resource.Id.cookieText);
            //_cookieImage.Click += CookieImageButton_Click;
            _cookieImage.Touch += CookieImageButton_Touch;
        }

        private void CookieImageButton_Click(object sender, System.EventArgs e)
        {
            _clicks++;
            _cookieText.Text = "Je hebt " + _clicks + " keer op dit nutteloze koekje geklikt";
        }

        private void CookieImageButton_Touch(object sender, View.TouchEventArgs e)
        {
            //e.Handled = false;
            if (e.Event.Action == MotionEventActions.Down)
            {
                _clicks++;
                _cookieText.Text = "Je hebt " + _clicks + " keer op dit nutteloze koekje geklikt";
                _cookieImage.SetImageResource(MemeCookie.Resource.Drawable.ImperfectCookie);
            }
            else if (e.Event.Action == MotionEventActions.Up)
            {
                _cookieImage.SetImageResource(MemeCookie.Resource.Drawable.PerfectCookie);
            }
        }
    }
}

